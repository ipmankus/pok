;====================================================================
; Processor		: ATmega8515
; Compiler		: AVRASM
;====================================================================

;====================================================================
; DEFINITIONS
;====================================================================

.include "m8515def.inc"

; Temporary registers
.def temp = r16 
.def temp_1 = r19
.def temp_2 = r18

; Necessary registers
.def is_playing = r17 
.def number_to_write = r20
.def number_generator = r21
.def time_remaining = r22
.def number_written = r23
.def PB = r24
.def number_counter_pos = r25

; Constants in LCD
.equ timer_pos = 138 

;====================================================================
; RESET and INTERRUPT VECTORS
;====================================================================

.org $00
rjmp MAIN
.org $01
rjmp START_GAME
.org $02
rjmp STOP_GAME
.org $07
rjmp ISR_TOV0

;====================================================================
; CODE SEGMENT
;====================================================================


MAIN:
	ldi is_playing, 0

INIT_STACK:
	ldi temp, low(RAMEND)
	ldi temp, high(RAMEND)
	out SPH, temp

INIT_INTERRUPT:
	ldi temp, 0b00001010
	out MCUCR, temp
	ldi temp, 0b11000000
	out GICR, temp
	sei

rcall INIT_LCD_MAIN

FOREVER:
	ldi XL, 0 ; player score
	ldi time_remaining, 10
	tst is_playing
	brne PLAYING
	rjmp NOT_PLAYING ; avoiding out of reach branch
	
	PLAYING:
		/* currently playing
		 * will execute keypad and stuff
		 */
		 INIT_TIMER:
		 	tst is_playing
			brne PLAYING_2
			rjmp NOT_PLAYING
			PLAYING_2:
			ldi temp, (1<<CS00|1<<CS01|1<<CS02) ; timer stopped at first
			out TCCR0, temp
			ldi temp,1<<TOV0
    		out TIFR,temp
			ldi temp,1<<TOIE0
    		out TIMSK,temp
			
			sei  
		                

		 INIT_GAME:
		 	ldi number_counter_pos, 192
		 	mov number_to_write, number_generator
			rcall CLEAR_LCD
			ldi temp_1, 1
			mov temp, number_to_write
			rcall INPUT_NUMBER_TO_WRITE
			ldi number_written, 0
			

		 INIT_KEYPAD:
		 	ldi temp, 0x00
			out DDRC, temp

		 START_TIMER:
		 	rcall WRITE_TIMER 
		 	ldi temp, (1<<CS00|1<<CS02)
			out TCCR0, temp
			rcall CHANGE_LCD_POINTER_TO_CUR_NUMBER_POS
			

		 PULLING_KEYPAD_ROW:
		 	inc number_generator ; for pseudo random purpose

		 	tst is_playing
			brne PLAYING_1 ; penting agar pulling ga jalan setelah game stop
			rjmp NOT_PLAYING
			PLAYING_1:

			ldi temp, 0b00001111 ; kirim supaya tahu coloumn yang high
			out DDRC, temp
			out PORTC, temp
			nop
			in temp, PINC
			cpi temp, 0b00001111

			brne A_KEY_WAS_PRESSED
		 	rjmp PULLING_KEYPAD_ROW

		 A_KEY_WAS_PRESSED:
			ldi temp_1, 0b11110000 ; kirim supaya tahu row yang high
			out DDRC, temp_1
			out PORTC, temp_1
			nop
			in temp_1, PINC
			and temp, temp_1

			cpi temp, 0b01000001
			breq KEY_0
			cpi temp, 0b10001000
			breq KEY_1
			cpi temp, 0b01001000
			breq KEY_2
			cpi temp, 0b00101000
			breq KEY_3
			cpi temp, 0b10000100
			breq KEY_4
			cpi temp, 0b01000100
			breq KEY_5
			cpi temp, 0b00100100
			breq KEY_6
			cpi temp, 0b10000010
			breq KEY_7
			cpi temp, 0b01000010
			breq KEY_8
			cpi temp, 0b00100010
			breq KEY_9
			cpi temp, 0b10000001
			breq KEY_CLEAR
			cpi temp, 0b00100001
			brne KEY_ENTER_BR
			rjmp KEY_ENTER
			KEY_ENTER_BR:
			rjmp PULLING_KEYPAD_ROW

			KEY_0:
				rcall MUL_10
				ldi temp_1, 0
				rcall INPUT_WRITE_0
				rjmp PULLING_KEYPAD_ROW
			KEY_1:
				rcall MUL_10
				ldi temp_1, 0
				subi number_written, -1
				rcall INPUT_WRITE_1
				rjmp PULLING_KEYPAD_ROW
			KEY_2:
				rcall MUL_10
				ldi temp_1, 0
				subi number_written, -2
				rcall INPUT_WRITE_2
				rjmp PULLING_KEYPAD_ROW
			KEY_3:
				rcall MUL_10
				ldi temp_1, 0
				subi number_written, -3
				rcall INPUT_WRITE_3
				rjmp PULLING_KEYPAD_ROW
			KEY_4:
				rcall MUL_10
				ldi temp_1, 0
				subi number_written, -4
				rcall INPUT_WRITE_4
				rjmp PULLING_KEYPAD_ROW
			KEY_5:
				rcall MUL_10
				ldi temp_1, 0
				subi number_written, -5
				rcall INPUT_WRITE_5
				rjmp PULLING_KEYPAD_ROW
			KEY_6:
				rcall MUL_10
				ldi temp_1, 0
				subi number_written, -6
				rcall INPUT_WRITE_6
				rjmp PULLING_KEYPAD_ROW
			KEY_7:
				rcall MUL_10
				ldi temp_1, 0
				subi number_written, -7
				rcall INPUT_WRITE_7
				rjmp PULLING_KEYPAD_ROW
			KEY_8:
				rcall MUL_10
				ldi temp_1, 0
				subi number_written, -8
				rcall INPUT_WRITE_8
				rjmp PULLING_KEYPAD_ROW
			KEY_9:
				rcall MUL_10
				ldi temp_1, 0
				subi number_written, -9
				rcall INPUT_WRITE_9
				rjmp PULLING_KEYPAD_ROW
			KEY_CLEAR:
				/*
				* clear lcd then input timer and num to write again
				*/
				ldi temp, (1<<CS00|1<<CS01|1<<CS02) 
				out TCCR0, temp
				ldi number_counter_pos, 192
				ldi number_written, 0
				rcall CLEAR_LCD
				mov temp, number_to_write
				rcall INPUT_NUMBER_TO_WRITE
				rcall WRITE_TIMER
				rcall CHANGE_LCD_POINTER_TO_NEWLINE
				ldi temp, (1<<CS00|1<<CS02)
				out TCCR0, temp 
				rjmp PULLING_KEYPAD_ROW

			KEY_ENTER:
				/*
				* check if number is the same, blink led, add timer, do stuff.
				*/
				cp number_written, number_to_write
				brne INPUT_DIFFER
				rcall BLINK_WIN_LED
				subi time_remaining, -3
				inc XL
				rjmp PLAYING
				INPUT_DIFFER:
				rcall BLINK_LOSE_LED
				rjmp PLAYING
	NOT_PLAYING:
	inc number_generator
	rjmp FOREVER

WRITE_TIMER:
	rcall CHANGE_LCD_POINTER_TO_TIMER_POS
	mov temp, time_remaining
	ldi temp_1, 1
	rcall INPUT_NUMBER_TO_WRITE
	ret 

MUL_10:
	inc number_counter_pos
	mov temp, number_written
	lsl temp
	lsl temp
	add temp, number_written
	lsl temp
	mov number_written, temp 
	ret

INPUT_NUMBER_TO_WRITE:
	; check bigger than 0x64, 0xA
	; Proof of concept only, number is overflowed, and max len=3 (8bit)
	cpi temp, 200
	brsh INPUT_NUMBER_TO_WRITE_BR

	cpi temp,100
	brsh INPUT_NUMBER_TO_WRITE_BR_1

	rcall INPUT_WRITE_0
	INPUT_NUMBER_TO_WRITE_BACK:

	ldi temp_1, 10
	ldi temp_2, 1
	LOOP_TEST_LT_10:
		cp temp, temp_1
		brlo OUT_LT_10
		inc temp_2
		subi temp_1, -10
		rjmp LOOP_TEST_LT_10
	OUT_LT_10:
	subi temp_2, 1
	subi temp_1, 10
	sub temp, temp_1

	rcall CHOOSE_WRITE_WHAT

	ldi temp_1, 1
	ldi temp_2, 1
	LOOP_TEST_LT_1:
		cp temp, temp_1
		brlo OUT_LT_1
		inc temp_2
		subi temp_1, -1
		rjmp LOOP_TEST_LT_1
	OUT_LT_1:
	subi temp_2, 1
	subi temp_1, 1
	sub temp, temp_1

	rcall CHOOSE_WRITE_WHAT
	ret



INPUT_NUMBER_TO_WRITE_BR:
	subi temp,200
	rcall INPUT_WRITE_2
	rjmp INPUT_NUMBER_TO_WRITE_BACK

INPUT_NUMBER_TO_WRITE_BR_1:
	subi temp,100
	rcall INPUT_WRITE_1
	rjmp INPUT_NUMBER_TO_WRITE_BACK

CHOOSE_WRITE_WHAT:
	push temp

	cpi temp_2, 9
	brne NEXT_CHOOSE_WRITE_WHAT
	rcall INPUT_WRITE_9
	NEXT_CHOOSE_WRITE_WHAT:
	
	cpi temp_2, 8
	brne NEXT_CHOOSE_WRITE_WHAT_1
	rcall INPUT_WRITE_8
	NEXT_CHOOSE_WRITE_WHAT_1:	

	cpi temp_2, 7
	brne NEXT_CHOOSE_WRITE_WHAT_2
	rcall INPUT_WRITE_7
	NEXT_CHOOSE_WRITE_WHAT_2:

	cpi temp_2, 6
	brne NEXT_CHOOSE_WRITE_WHAT_3
	rcall INPUT_WRITE_6
	NEXT_CHOOSE_WRITE_WHAT_3:
	
	cpi temp_2, 5
	brne NEXT_CHOOSE_WRITE_WHAT_4
	rcall INPUT_WRITE_5
	NEXT_CHOOSE_WRITE_WHAT_4:

	cpi temp_2, 4
	brne NEXT_CHOOSE_WRITE_WHAT_5
	rcall INPUT_WRITE_4
	NEXT_CHOOSE_WRITE_WHAT_5:
		
	cpi temp_2, 3
	brne NEXT_CHOOSE_WRITE_WHAT_6
	rcall INPUT_WRITE_3
	NEXT_CHOOSE_WRITE_WHAT_6:
	
	cpi temp_2, 2
	brne NEXT_CHOOSE_WRITE_WHAT_7
	rcall INPUT_WRITE_2
	NEXT_CHOOSE_WRITE_WHAT_7:

	cpi temp_2, 1
	brne NEXT_CHOOSE_WRITE_WHAT_8
	rcall INPUT_WRITE_1
	NEXT_CHOOSE_WRITE_WHAT_8:
	
	cpi temp_2, 0
	brne NEXT_CHOOSE_WRITE_WHAT_9
	rcall INPUT_WRITE_0
	NEXT_CHOOSE_WRITE_WHAT_9:

	pop temp 
	ret

BLINK_WIN_LED:
	sbi PORTA,3
	rcall DELAY_01
	cbi PORTA, 3
	rcall DELAY_01
	sbi PORTA,3
	rcall DELAY_01
	cbi PORTA, 3
	rcall DELAY_01
	sbi PORTA,3
	rcall DELAY_01
	cbi PORTA, 3
	ret

BLINK_LOSE_LED:
	sbi PORTA,4
	rcall DELAY_01
	cbi PORTA, 4
	rcall DELAY_01
	sbi PORTA,4
	rcall DELAY_01
	cbi PORTA, 4
	rcall DELAY_01
	sbi PORTA,4
	rcall DELAY_01
	cbi PORTA, 4
	ret

START_GAME:
	ldi is_playing, 1
	rcall DISPLAY_START
	reti

DISPLAY_START:
	rcall INPUT_MESSAGE
	rcall DELAY_02
	rcall DELAY_02
	rcall CLEAR_LCD
	rcall INPUT_MESSAGE_2
	rcall DELAY_02
	rcall DELAY_02
	
	ret

STOP_GAME:
	ldi is_playing, 0
	ldi temp, (1<<CS00|1<<CS01|1<<CS02) 
	out TCCR0, temp
	rcall CLEAR_LCD	
	reti

ISR_TOV0:
	push temp
	push temp_1
	dec time_remaining
	tst time_remaining
	brne NOT_OVER_TOV0
		ldi temp, (1<<CS00|1<<CS01|1<<CS02) 
		out TCCR0, temp
		rcall CLEAR_LCD
		rcall INPUT_MESSAGE_TIMES_UP
		rcall CLEAR_LCD
		rcall INPUT_FINAL
		mov temp, XL
		rcall INPUT_NUMBER_TO_WRITE
		ldi is_playing, 0
		
		pop temp_1
		pop temp
		reti
	NOT_OVER_TOV0:
	rcall WRITE_TIMER
	rcall CHANGE_LCD_POINTER_TO_CUR_NUMBER_POS
	pop temp_1
	pop temp
    reti

DELAY_00:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; Delay 4 000 cycles
	; 500us at 8.0 MHz
	push r19
	push r18
	    ldi  r18, 6
	    ldi  r19, 49
	L0: dec  r19
	    brne L0
	    dec  r18
	    brne L0
	pop r18
	pop r19
	ret

DELAY_01:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; DELAY_CONTROL 40 000 cycles
	; 5ms at 8.0 MHz
	push r19
	push r18
	    ldi  r18, 52
	    ldi  r19, 242
	L1: dec  r19
	    brne L1
	    dec  r18
	    brne L1
	    nop
	pop r18
	pop r19
	ret

DELAY_02:
; Generated by delay loop calculator
; at http://www.bretmulvey.com/avrdelay.html
;
; Delay 160 000 cycles
; 20ms at 8.0 MHz
	push r19
	push r18
	    ldi  r18, 208
	    ldi  r19, 202
	L2: dec  r19
	    brne L2
	    dec  r18
	    brne L2
	    nop
	pop r18
	pop r19
	ret

INPUT_FINAL:
	ldi ZH, high(2*message_final)
	ldi ZL, low(2*message_final)
	rcall LOADBYTE
	ret

INPUT_PLAY_AGAIN:
	ldi ZH, high(2*message_play_again)
	ldi ZL, low(2*message_play_again)
	rcall LOADBYTE
	ret

INPUT_WRITE_1:
	ldi ZH, high(2*num_1)
	ldi ZL, low(2*num_1)
	rcall LOADBYTE
	ret

INPUT_WRITE_2:
	ldi ZH, high(2*num_2)
	ldi ZL, low(2*num_2)
	rcall LOADBYTE
	ret

INPUT_WRITE_3:
	ldi ZH, high(2*num_3)
	ldi ZL, low(2*num_3)
	rcall LOADBYTE
	ret

INPUT_WRITE_4:
	ldi ZH, high(2*num_4)
	ldi ZL, low(2*num_4)
	rcall LOADBYTE
	ret

INPUT_WRITE_5:
	ldi ZH, high(2*num_5)
	ldi ZL, low(2*num_5)
	rcall LOADBYTE
	ret

INPUT_WRITE_6:
	ldi ZH, high(2*num_6)
	ldi ZL, low(2*num_6)
	rcall LOADBYTE
	ret

INPUT_WRITE_7:
	ldi ZH, high(2*num_7)
	ldi ZL, low(2*num_7)
	rcall LOADBYTE
	ret

INPUT_WRITE_8:
	ldi ZH, high(2*num_8)
	ldi ZL, low(2*num_8)
	rcall LOADBYTE
	ret

INPUT_WRITE_9:
	ldi ZH, high(2*num_9)
	ldi ZL, low(2*num_9)
	rcall LOADBYTE
	ret

INPUT_WRITE_0:
	ldi ZH, high(2*num_0)
	ldi ZL, low(2*num_0)
	rcall LOADBYTE
	ret

INPUT_MESSAGE:
	ldi temp_1, 0
	ldi ZH,high(2*message_welcome) ; Load high part of byte address into ZH
	ldi ZL,low(2*message_welcome) ; Load low part of byte address into ZL
	rcall LOADBYTE
	ldi ZH,high(2*message_welcome_2) ; Load high part of byte address into ZH
	ldi ZL,low(2*message_welcome_2) ; Load low part of byte address into ZL
	rcall CHANGE_LCD_POINTER_TO_NEWLINE
	rcall LOADBYTE
	ret

INPUT_MESSAGE_2:
	ldi temp_1, 0
	ldi ZH,high(2*message_instruct) ; Load high part of byte address into ZH
	ldi ZL,low(2*message_instruct) ; Load low part of byte address into ZL
	rcall LOADBYTE
	ldi ZH,high(2*message_instruct_2) ; Load high part of byte address into ZH
	ldi ZL,low(2*message_instruct_2) ; Load low part of byte address into ZL
	rcall CHANGE_LCD_POINTER_TO_NEWLINE
	rcall LOADBYTE
	ret

INPUT_MESSAGE_TIMES_UP:
	ldi temp_1, 0
	ldi ZH, high(2*message_times_up)
	ldi ZL, low(2*message_times_up)
	rcall LOADBYTE
	ret

CHANGE_LCD_POINTER_TO_NEWLINE:
	ldi temp, 192
	out PORTB, temp
	cbi PORTA, 1
	cbi PORTA, 2
	sbi PORTA, 0
	cbi PORTA, 0
	ret 

CHANGE_LCD_POINTER_TO_TIMER_POS:
	ldi temp, 138
	out PORTB, temp
	cbi PORTA, 1
	cbi PORTA, 2
	sbi PORTA, 0
	cbi PORTA, 0
	ret 

CHANGE_LCD_POINTER_TO_CUR_NUMBER_POS:
	out PORTB, number_counter_pos
	cbi PORTA, 1
	cbi PORTA, 2
	sbi PORTA, 0
	cbi PORTA, 0
	ret 



INIT_LCD_MAIN:
	rcall INIT_LCD

	ser temp
	out DDRA,temp ; Set port A as output
	out DDRB,temp ; Set port B as output
	ret

LOADBYTE:
	lpm ; Load byte from program memory into r0
	tst r0 ; Check if we've reached the end of the message
	breq END_LCD ; If so, quit
	rcall WRITE_TEXT
	adiw ZL,1 ; Increase Z registers
	rjmp LOADBYTE




END_LCD:
	ret

INIT_LCD:
	cbi PORTA,1 ; CLR RS
	ldi PB,0x38 ; MOV DATA,0x38 --> 8bit, 2line, 5x7
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	cbi PORTA,1 ; CLR RS
	ldi PB,$0E ; MOV DATA,0x0E --> disp ON, cursor ON, blink OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	rcall CLEAR_LCD ; CLEAR LCD
	cbi PORTA,1 ; CLR RS
	ldi PB,$06 ; MOV DATA,0x06 --> increase cursor, display sroll OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret

CLEAR_LCD:
	cbi PORTA,1 ; CLR RS
	ldi PB,$01 ; MOV DATA,0x01
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret

WRITE_TEXT:
	sbi PORTA,1 ; SETB RS
	out PORTB, r0
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	tst temp_1
	brne WRITE_TEXT_NO_DELAY
	rcall DELAY_01
	WRITE_TEXT_NO_DELAY:
	ret

;====================================================================
; DATA
;====================================================================

message_welcome:
.db "WELCOME TO THE '-' GAME!  ",1
.db 0

message_welcome_2:
.db "By: Steven,Agas,Sophie"
.db 0

message_instruct:
.db "TO PLAY, PRESS THE UPCOMING"
.db 0

message_instruct_2:
.db "NUMBER FROM THE KEYPAD"
.db 0

message_final:
.db "Your Score : "
.db 0

message_play_again:
.db "Play Again ? "
.db 0

message_times_up:
.db "TIMES UP!"
.db 0

num_1:
.db "1"
.db 0

num_2:
.db "2"
.db 0

num_3:
.db "3"
.db 0

num_4:
.db "4"
.db 0

num_5:
.db "5"
.db 0

num_6:
.db "6"
.db 0

num_7:
.db "7"
.db 0

num_8:
.db "8"
.db 0

num_9:
.db "9"
.db 0

num_0:
.db "0"
.db 0
