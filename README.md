# POK Tugas Akhir

## Getting Started



### Prerequisites

Things you need to install:

```
AVR Studio 4
Hapsim
```

### How to run ?

Create a new project in AVR Studio 4, and copy/open TA.asm into the text editor. 

To start debuggin mode, press
```
Build and Run (Ctrl + F7)  
```
Then open hapsim and load the xml configuration, change to AVR again, and press
```
Run (F5)
```

## Authors


* **Abicantya Prasidya Sophie** - *1306387922*
* **I Gusti Putu Agastya Indrayana** - *1706074940* 
* **Steven Kusuman** - *1706028676*